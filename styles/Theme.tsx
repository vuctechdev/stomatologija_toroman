import React, { FC, ReactElement } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { Montserrat, Lora, Allura } from "next/font/google";

const LoraFont = Lora({
  style: ["normal"],
  weight: ["400", "500", "600", "700"],
  subsets: ["latin"],
  preload: true,
});

const MontserratFont = Montserrat({
  style: ["normal"],
  weight: ["400", "500", "600", "700"],
  subsets: ["latin"],
  preload: true,
});

const AlluraFont = Allura({
  style: ["normal"],
  weight: ["400"],
  subsets: ["latin"],
  preload: true,
});

const md = 760;

declare module "@mui/material/styles" {
  interface TypographyVariants {
    dr: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    dr: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    dr: true;
  }
}

let theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 760,
      md: 1024,
      lg: 1500,
      xl: 2000,
    },
  },
  palette: {
    mode: "light",
    primary: {
      dark: "#d2bdb3",
      main: "#e9d2c7",
      light: "#e9d2c74d",
    },
    secondary: {
      main: "#80967c",
    },
    background: {
      default: "#fff",
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          padding: "12px 24px",
        },
      },
    },
  },
  typography: {
    allVariants: {
      fontFamily: LoraFont.style.fontFamily,
      fontVariant: LoraFont.style.fontStyle,
      textAlign: "center",
      lineHeight: 1.3,
      color: "#010101",
    },
    dr: {
      fontFamily: AlluraFont.style.fontFamily,
      fontVariant: AlluraFont.style.fontStyle,
      textAlign: "center",
      lineHeight: 1.3,
      color: "#010101",
      fontSize: "48px",
      fontWeight: 500,
      [`@media (max-width:${md}px)`]: {
        fontSize: "38px",
      },
    },
    h1: {
      fontSize: "62px",
      fontWeight: 700,
      fontFamily: AlluraFont.style.fontFamily,
      fontVariant: AlluraFont.style.fontStyle,
      [`@media (max-width:${md}px)`]: {
        fontSize: "32px",
        fontWeight: 600,
      },
    },
    h2: {
      fontSize: "48px",
      fontWeight: 700,
      // fontFamily: AlluraFont.style.fontFamily,
      // fontVariant: AlluraFont.style.fontStyle,
      [`@media (max-width:${md}px)`]: {
        fontSize: "28px",
      },
    },
    h3: {
      fontSize: "36px",
      fontWeight: 600,
      [`@media (max-width:${md}px)`]: {
        fontSize: "24px",
      },
    },
    h4: {
      fontSize: "32px",
      fontWeight: 500,
      [`@media (max-width:${md}px)`]: {
        fontSize: "20px",
      },
    },
    h5: {
      fontSize: "28px",
      fontWeight: 500,
      [`@media (max-width:${md}px)`]: {
        fontSize: "18px",
      },
    },
    h6: {
      fontSize: "20px",
      fontWeight: 500,
      [`@media (max-width:${md}px)`]: {
        fontSize: "18px",
      },
    },
    body1: {
      fontFamily: MontserratFont.style.fontFamily,
      fontVariant: MontserratFont.style.fontStyle,
      fontSize: "20px",
      fontWeight: 400,
      [`@media (max-width:${md}px)`]: {
        fontSize: "14px",
      },
    },
    body2: {
      fontFamily: MontserratFont.style.fontFamily,
      fontVariant: MontserratFont.style.fontStyle,
      fontSize: "16px",
      fontWeight: 400,
      [`@media (max-width:${md}px)`]: {
        fontSize: "12px",
      },
    },
    button: {
      color: "#FFFFFF",
      fontSize: "20px",
      lineHeight: 1.3,
      fontWeight: 600,
      letterSpacing: "0.17em",
      transition: "all .3s",
      // [`@media (max-width:${md}px)`]: {
      //   fontSize: "18px",
      // },
    },
  },
});

interface ThemeProvider2Props {
  children: any;
}

const ThemeProvider2: FC<ThemeProvider2Props> = ({
  children,
}): ReactElement => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default ThemeProvider2;
