/** @type {import('next').NextConfig} */
// const withBundleAnalyzer = require("@next/bundle-analyzer")({
//   enabled: process.env.ANALYZE !== "true",
// });

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    unoptimized: true,
  },
  compress: true,
  // async headers() {
  //   return [
  //     {
  //       // Set a Cache-Control header for static assets (e.g., images, stylesheets)
  //       source: '/images/:path*',
  //       headers: [
  //         {
  //           key: 'Cache-Control',
  //           value: 'public, max-age=31536000, immutable', // Adjust the max-age value as needed
  //         },
  //       ],
  //     },
  //   ];
  // },
};

module.exports = nextConfig;
