export const getFlexSX = () => {
    return {
        display: "flex", 
        flexDirection: "column",
        justifyContent: "center"
    }
}