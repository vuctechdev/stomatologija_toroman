import { apiBaseUrl } from "../common/data/config";

export interface InsertAnalytics {
  eventGroup: string;
  eventName: string;
  newVisitor: string;
}

export const ANALYTICS_GROUPS = {
  PAGE_VIEW: "PAGE_VIEW",
  PROJECT_CLICK: "PROJECT_CLICK",
  SOCIAL_CLICK: "SOCIAL_CLICK",
};

export const handleAnalytics = (code: number) => {
  if (!!global.window) {
    const oldVisitor = global.window.localStorage.getItem("oldVisitor");
    const session = global.window.sessionStorage.getItem("session");
    if (code < 2000 && !session) {
      global.window.sessionStorage.setItem("session", "true");
    } else if (code < 2000) {
      return;
    }
    if (!oldVisitor) {
      code += 100;
      global.window.localStorage.setItem("oldVisitor", "true");
    }
    insertAnalytics(code);
  }
};

export const insertAnalytics = async (code: number) => {
  try {
    fetch(`${apiBaseUrl}/analytics`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      method: "POST",
      body: JSON.stringify({ code }),
    });
  } catch (err) {
    console.log(err);
  }
};
