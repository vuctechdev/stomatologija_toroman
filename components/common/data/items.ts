// export const services = [
//   "Enterijer",
//   "Eksterijer",
//   "3D Modelovanje",
//   "Projektovanje",
//   "Nadzor",
//   "Savjetovanje",
// ];

export const services = [
  {
    title: "Opšta stomatologija",
    items: [
      "Kompozitne plombe",
      "Liječenje kanala korijena (ručno i mašinski)",
    ],
  },
  {
    title: "Ortodoncija",
    items: [
      "Fiksni i mobilni aparatići za ispravljanje zuba",
      "Folije za ispravljanje zuba",
    ],
  },
  {
    title: "Stomatološka protetika",
    items: [
      "Parcijalne i totalne proteze",
      "Kombinovani radovi",
      "Metalokeramičke krune",
      "Bezmetalne cirkonijum krune",
    ],
  },
  {
    title: "Parodontologija ",
    items: [
      "Uklanjanje tvrdih zubnih naslaga-kamenca",
      "Uklanjanje plaka-mekih naslaga na zubima",
      "Pjeskarenje zuba-uklanjanje pigmentacija",
      "Kiretaza gingivalnih džepova",
    ],
  },
  // {
  //   title: "Endodoncija",
  //   items: ["Liječenje kanala korijena", "Ručno i mašinski"],
  // },
  {
    title: "Estetska stomatologija",
    items: [
      "Kompozitne fasete",
      "Izbjeljivanje vitalnih zuba Fläsh lampom",
      "Izbjeljivanje avitalnih zuba",
      "Kućno izbjeljivanje zuba",
    ],
  },
  {
    title: "Dječija i preventivna stomatologija",
    items: ["Fluorizacija zuba", "Zalivanje fisura"],
  },
  {
    title: "Oralna hirurgija sa implantologijom",
    items: [
      "Hirursko vađenje zuba",
      "Apikotomija",
      "Ugradnja implantata",
      "All on 4",
      "All on 6",
      "Sinus lift",
      "Augmentacija kosti",
    ],
  },
];

export const otherProjects = [
  // {
  //   _id: "13",
  //   top: true,
  //   area: "65",
  //   location: "Banja Luka",
  //   date: "Oktobar 2023",
  //   description: "Uredjenje enterijera u stambenom objektu.",
  // },
  {
    _id: "12",
    area: "51",
    location: "Banja Luka",
    date: "Avgust 2023",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  // {
  //   _id: "11",
  //   top: true,
  //   area: "78",
  //   location: "Beograd",
  //   date: "Jun 2023",
  //   description: "Uredjenje enterijera u stambenom objektu.",
  // },
  {
    _id: "10",
    area: "120",
    location: "Banja Luka",
    date: "Mart 2023",
    description: "Uredjenje enterijera u obrazovnoj ustanovi.",
  },
  {
    _id: "09",
    area: "74",
    location: "Banja Luka",
    date: "Januar 2023",
    description: "Uredjenje enterijera u stomatološkoj ordinaciji.",
  },
  {
    _id: "08",
    area: "43",
    location: "Banja Luka",
    date: "Novembar 2022",
    description: "Uredjenje enterijera u cvjećari.",
  },
  {
    _id: "07",
    area: "48",
    location: "Banja Luka",
    date: "Septembar 2022",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  {
    _id: "06",
    area: "25",
    location: "Banja Luka",
    date: "Jul 2022",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  // {
  //   _id: "05",
  //   top: true,
  //   area: "300",
  //   location: "Banja Luka",
  //   date: "Jun 2022",
  //   description: "Uredjenje eksterijera na benzinskoj pumpi.",
  // },
  {
    _id: "04",
    area: "156",
    location: "Banja Luka",
    date: "Mart 2022",
    description: "Uredjenje enterijera u ugostiteljskom objektu.",
  },
  {
    _id: "03",
    area: "54",
    location: "Banja Luka",
    date: "Decembar 2021",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  {
    _id: "02",
    area: "41",
    location: "Banja Luka",
    date: "Avgust 2021",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  {
    _id: "01",
    area: "50",
    location: "Banja Luka",
    date: "Februar 2021",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
];

export const highlightedProjects = [
  {
    _id: "13",
    area: "65",
    location: "Banja Luka",
    date: "Oktobar 2023",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  {
    _id: "11",
    area: "78",
    location: "Beograd",
    date: "Jun 2023",
    description: "Uredjenje enterijera u stambenom objektu.",
  },
  {
    _id: "05",
    area: "300",
    location: "Banja Luka",
    date: "Jun 2022",
    description: "Uredjenje eksterijera na benzinskoj pumpi.",
  },
];
