export const mediaBaseURL = "https://mtarchitecture.net/images";
// export const apiBaseUrl = "http://localhost:2302/api";
export const apiBaseUrl = "https://stomatologijatoroman.com/api";

export const title = "Stomatologija Toroman, Banja Luka";
export const description =
  "Otkrijte vrhunsku stomatološku njegu. Profesionalna usluga, prijateljski tim i najnovija tehnologija za Vaš savršeni osmijeh.";
export const favicon = "/images/favicon.png";
export const webURL = "https://stomatologijatoroman.com";
