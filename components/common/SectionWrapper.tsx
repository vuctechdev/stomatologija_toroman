import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

interface SectionWrapperProps {
  title: string;
  id?: string;
  children: React.ReactElement;
}

const SectionWrapper: FC<SectionWrapperProps> = ({
  children,
  title,
  id,
}): ReactElement => {
  return (
    <Box
      id={id}
      width={1}
      sx={(theme) => ({
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        pt: "120px",
        px: "40px",
        [theme.breakpoints.down("sm")]: {
          pt: "60px",
          px: "20px",
        }
      })}
    >
      <Typography variant="h2" textAlign="center">
        {title.toUpperCase()}
      </Typography>
      <Box
        sx={(theme) => ({
          backgroundColor: theme.palette.secondary.main,
          height: "4px",
          width: "230px",
          mb: "70px",
          [theme.breakpoints.down("sm")]: {
            mb: "40px",
          }
        })}
      ></Box>
      <Box
        width={1}
        maxWidth="1248px"
        sx={(theme) => ({
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        })}
      >
        {children}
      </Box>
    </Box>
  );
};

export default SectionWrapper;
