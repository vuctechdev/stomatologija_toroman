import TextField from "@mui/material/TextField";
import { useFormikContext } from "formik";
import React, { FC, ReactElement, useEffect, useState } from "react";
import { InitialValues } from "./AppointmentForm";
import { services } from "./data/items";
import MenuItem from "@mui/material/MenuItem";
import { Typography } from "@mui/material";

interface FormFieldsProps {}

const FormFields: FC<FormFieldsProps> = (): ReactElement => {
  const [subservices, setSubservices] = useState<string[]>([]);
  const { touched, errors, getFieldProps, values } =
    useFormikContext<InitialValues>();

  const getErrorMessage = (
    name: "service" | "subservice" | "phone" | "name"
  ) => {
    if (touched[name] && errors[name]) {
      return errors[name];
    }
    return "";
  };

  useEffect(() => {
    const data: string[] =
      services.find((item) => item?.title === values?.service)?.items ?? [];
    setSubservices(
      values?.service === "Opšta stomatologija" ? ["Pregled", ...data] : data
    );
  }, [values?.service]);

  const displaySubservis = !!values?.service && !!subservices.length;

  return (
    <>
      <TextField
        fullWidth
        label="Ime i prezime"
        {...getFieldProps("name")}
        error={touched.name && !!errors.name}
        helperText={
          <Typography variant="body2" color="error" textAlign="left">
            {getErrorMessage("name")}
          </Typography>
        }
        sx={{
          mb: "22px",
        }}
      />
      <TextField
        fullWidth
        select
        label="Izaberite uslugu"
        {...getFieldProps("service")}
        error={touched.service && !!errors.service}
        helperText={
          <Typography variant="body2" color="error" textAlign="left">
            {getErrorMessage("service")}
          </Typography>
        }
        sx={{
          mb: "22px",
          "& div": {
            textAlign: "left",
          },
        }}
      >
        {services.map(({ title }) => {
          return (
            <MenuItem key={title} value={title}>
              {title}
            </MenuItem>
          );
        })}
      </TextField>
      {displaySubservis && (
        <TextField
          fullWidth
          select
          label="Izaberite tačnu uslugu"
          {...getFieldProps("subservice")}
          error={touched.subservice && !!errors.subservice}
          helperText={
            <Typography variant="body2" color="error" textAlign="left">
              {getErrorMessage("subservice")}
            </Typography>
          }
          sx={{
            mb: "22px",
            "& div": {
              textAlign: "left",
            },
          }}
        >
          {subservices.map((item) => {
            return (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            );
          })}
          <MenuItem value="Drugo">Drugo</MenuItem>
        </TextField>
      )}
      <TextField
        fullWidth
        label="Broj telefona"
        {...getFieldProps("phone")}
        error={touched.phone && !!errors.phone}
        helperText={
          <Typography variant="body2" color="error" textAlign="left">
            {getErrorMessage("phone")}
          </Typography>
        }
        sx={{
          mb: "22px",
        }}
      />
    </>
  );
};

export default FormFields;
