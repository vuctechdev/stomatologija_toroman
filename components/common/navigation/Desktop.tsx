import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import AppointmentButton from "../AppointmentButton";

interface DesktopNavigationProps {
  sections: { title: string; sr: string }[];
}

const DesktopNavigation: FC<DesktopNavigationProps> = ({
  sections,
}): ReactElement => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
          maxWidth: "1248px",
          flexGrow: 1,
        }}
      >
        {sections.map((section) => (
          <Box
            key={section.title}
            sx={(theme) => ({
              mr: "30px",
              "& a": {
                textDecoration: "none",
              },
              [theme.breakpoints.down("sm")]: {
                fontSize: "14px",
                margin: "0px 10px",
              },
            })}
          >
            <a href={`/#${section.title}`}>
              <Typography variant="h6">
                {section.sr.toLocaleUpperCase()}
              </Typography>
            </a>
          </Box>
        ))}
      </Box>
      <AppointmentButton small />
    </>
  );
};

export default DesktopNavigation;
