import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import React, { FC, ReactElement, useState } from "react";
import { Hidden, IconButton } from "@mui/material";
import dynamic from "next/dynamic";
import MenuIcon from "@mui/icons-material/Menu";

const DesktopNavigation = dynamic(() => import("./Desktop"));
const MobileNavigation = dynamic(() => import("./Mobile"));

export const sections = [
  {
    title: "about",
    sr: "O Nama",
  },
  {
    title: "services",
    sr: "Usluge",
  },
  {
    title: "team",
    sr: "Naš tim",
  },
  {
    title: "contact",
    sr: "kontakt",
  },
];

const Navigation: FC = (): ReactElement => {
  const [open, setOpen] = useState(false);
  return (
    <AppBar
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "70px",
        backgroundColor: "#fff",
        flexDirection: "row",
        px: "20px",
      }}
      elevation={2}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
          width: "100%",
          maxWidth: "1248px",
        }}
      >
        <Hidden smDown>
          <DesktopNavigation sections={sections} />
        </Hidden>
        <Hidden smUp>
          <IconButton
            onClick={() => setOpen(true)}
            sx={{ color: "#000" }}
            aria-label="Menu"
          >
            <MenuIcon fontSize="large" />
          </IconButton>
          {open && <MobileNavigation setOpen={setOpen} sections={sections} />}
        </Hidden>
      </Box>
    </AppBar>
  );
};

export default Navigation;
