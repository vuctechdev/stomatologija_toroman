import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { Dialog, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import AppointmentButton from "../AppointmentButton";

interface MobileNavigationProps {
  sections: { title: string; sr: string }[];
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const MobileNavigation: FC<MobileNavigationProps> = ({
  sections,
  setOpen,
}): ReactElement => {
  return (
    <Dialog open={true} fullScreen>
      <IconButton
        size="large"
        onClick={() => setOpen(false)}
        sx={{
          position: "absolute",
          left: "16px",
          top: "6px",
          color: "#000",
        }}
      >
        <CloseIcon fontSize="large" />
      </IconButton>
      <Box
        height={1}
        sx={(theme) => ({
          backgroundColor: theme.palette.primary.light,
          display: "flex",
          flexDirection: "column",
          p: "90px 0px 50px",
        })}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flexGrow: 1,
          }}
          onClick={() => setOpen(false)}
        >
          {sections.map((section) => (
            <Box
              key={section.title}
              sx={(theme) => ({
                mb: "32px",
                "& a": {
                  textDecoration: "none",
                },
              })}
            >
              <a href={`/#${section.title}`}>
                <Typography variant="h2">
                  {section.sr.toLocaleUpperCase()}
                </Typography>
              </a>
            </Box>
          ))}
        </Box>
        <AppointmentButton small />
      </Box>
    </Dialog>
  );
};

export default MobileNavigation;
