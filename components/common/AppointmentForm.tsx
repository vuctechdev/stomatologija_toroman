import React, { FC, ReactElement, useState } from "react";
import Box from "@mui/material/Box";
import * as Yup from "yup";
import Grid from "@mui/material/Grid";
import { Form, Formik } from "formik";
import FormFields from "./FormFields";
import Button from "@mui/material/Button";
import { CircularProgress, Typography } from "@mui/material";
import { apiBaseUrl } from "./data/config";
import CheckBoxRoundedIcon from "@mui/icons-material/CheckBoxRounded";
import { handleAnalytics } from "../utils/analytics";

interface AppointmentFormProps {}

const initialValues = {
  service: "",
  subservice: "",
  phone: "",
  name: "",
};

export type InitialValues = typeof initialValues;

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Obavezno polje"),
  service: Yup.string().required("Obavezno polje"),
  subservice: Yup.string(),
  phone: Yup.string()
    .required("Obavezno polje")
    .matches(/^(06|05)\d{7,8}$/, "Očekivani format 0xx.."),
});

const AppointmentForm: FC<AppointmentFormProps> = (): ReactElement => {
  const [sent, setSent] = useState(false);

  const handleSubmit = async (values: InitialValues) => {
    try {
      handleAnalytics(2002);
      await fetch(`${apiBaseUrl}/contact`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        method: "POST",
        body: JSON.stringify(values),
      });
      setSent(true);
    } catch (err) {}
  };
  return (
    <Box
      sx={(theme) => ({
        width: "100%",
        height: "98vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        px: "20px",
        pt: "30px",
      })}
    >
      <Typography sx={{ mb: "40px", maxWidth: "500px" }}>
        Nakon primanja Vašeg upita, brzo ćemo Vas kontaktirati telefonom kako
        bismo odgovorili na sva pitanja i dogovorili detalje.
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form>
            <Grid
              container
              sx={(theme) => ({
                width: "100%",
                maxWidth: "400px",
              })}
            >
              <FormFields />
              <Box display="flex" width={1} justifyContent="flex-end" mt="14px">
                {sent ? (
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      width: "100%",
                      height: "50px",
                    }}
                  >
                    <Typography variant="body2" sx={{ display: "inline" }}>
                      Vaš upit je uspješno poslat
                    </Typography>
                    <CheckBoxRoundedIcon color="success" sx={{ ml: "4px" }} />
                  </Box>
                ) : (
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ minWidth: "150px" }}
                  >
                    {isSubmitting ? (
                      <CircularProgress size={24} color="inherit" />
                    ) : (
                      "Zakaži"
                    )}
                  </Button>
                )}
              </Box>
            </Grid>
          </Form>
        )}
      </Formik>
    </Box>
  );
};

export default AppointmentForm;
