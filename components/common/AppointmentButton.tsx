import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Link from "next/link";
import { handleAnalytics } from "../utils/analytics";

interface AppointmentButtonProps {
  small?: boolean;
}

const AppointmentButton: FC<AppointmentButtonProps> = ({
  small,
}): ReactElement => {
  return (
    <Box>
      <Link
        href="/appointment"
        onClick={() => handleAnalytics(small ? 2000 : 2001)}
      >
        <Button
          sx={{
            p: small ? "6px 16px" : "",
            zIndex: 101,
            position: "relative",
            flexShrink: 0,
          }}
          variant="contained"
        >
          Zakaži termin
        </Button>
      </Link>
    </Box>
  );
};

export default AppointmentButton;
