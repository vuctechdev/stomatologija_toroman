import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../common/SectionWrapper";
import { Typography } from "@mui/material";
import Image from "next/image";

interface AboutProps {}

const About: FC<AboutProps> = (): ReactElement => {
  return (
    <SectionWrapper title="O NAMA" id="about">
      <Box
        sx={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Typography maxWidth="860px" textAlign="center">
          Stomatološka ordinacija TOROMAN u svom novouređenom prostoru svakom
          pacijentu pruža, prije svega, kvalitetnu uslugu kao i detaljnu i
          stručnu brigu o zdravlju i ljepoti zuba. Individualnim pristupom
          dolazimo do najadekvatnijeg rješenja, poštujući želje pacijenata, ali
          istovremeno poštujući pravila struke. Naše ljubazno osoblje će Vam
          omogućiti da se osjećate prijatno.
        </Typography>
        
      </Box>
    </SectionWrapper>
  );
};

export default About;


