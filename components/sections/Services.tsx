import { Box, Typography } from "@mui/material";
import Grid from "@mui/material/Grid";
import React, { FC, ReactElement } from "react";
import SectionWrapper from "../common/SectionWrapper";
import { services } from "../common/data/items";

const Services: FC = (): ReactElement => {
  return (
    <SectionWrapper title="STOMATOLOšKE USLUGE" id="services">
      <Grid container justifyContent="center" flexWrap="wrap" width={1}>
        {services.map(({ title, items }, i) => (
          <Grid
            item
            xs={12}
            md={i !== 6 ? 6 : 12}
            key={title}
            sx={(theme) => ({
              px: "80px",
              mb: "90px",
              [theme.breakpoints.down("sm")]: { px: "0px", mb: "30px" },
            })}
          >
            <Typography variant="h3" textAlign="left">{`${title}`}</Typography>
            {items.map((item) => (
              <Box
                key={item}
                sx={(theme) => ({
                  mt: "8px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  pl: "28px",
                  [theme.breakpoints.down("sm")]: {
                    justifyContent: "flex-start",
                    pl: "18px",
                  },
                })}
              >
                <Box
                  sx={(theme) => ({
                    width: "8px",
                    height: "8px",
                    borderRadius: "4px",
                    mt: "2px",
                    mr: "8px",
                    backgroundColor: theme.palette.secondary.main,
                    [theme.breakpoints.down("sm")]: {
                      mt: "4px",
                      mr: "6px",
                    },
                  })}
                ></Box>
                <Typography variant="body2">{`${item}`}</Typography>
              </Box>
            ))}
          </Grid>
        ))}
      </Grid>
    </SectionWrapper>
  );
};

export default Services;
