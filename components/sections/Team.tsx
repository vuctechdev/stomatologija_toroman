import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../common/SectionWrapper";
import { Typography } from "@mui/material";
import Image from "next/image";

interface TeamProps {}

const Team: FC<TeamProps> = (): ReactElement => {
  return (
    <SectionWrapper title="Naš Tim" id="team">
      <>
        <Box
          sx={(theme) => ({
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
            width: "100%",
            mt: "0px",
            [theme.breakpoints.down("sm")]: {
              flexDirection: "column",
              justifyContent: "center",
            },
          })}
        >
          <Box
            sx={(theme) => ({
              position: "relative",
              width: "400px",
              height: "400px",
              borderRadius: "50%",
              overflow: "hidden",
              mr: "120px",
              [theme.breakpoints.down("md")]: {
                mr: "80px",
                width: "320px",
                height: "320px",
              },
              [theme.breakpoints.down("sm")]: {
                mr: "0px",
                width: "300px",
                height: "300px",
              },
            })}
          >
            {/* <picture>
          <source srcSet="/images/tijana.webp" media="(min-width: 420px)" /> */}
            <Image
              alt="dr Tanja Toroman"
              src={"/images/tijana.webp"}
              fill
              loading="lazy"
              style={{
                objectFit: "cover",
              }}
            />
            {/* </picture> */}
          </Box>
          <Typography
            variant="dr"
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                mt: "20px",
              },
            })}
          >
            dr stomatologije <br /> Tijana Toroman
          </Typography>
        </Box>
        <Box
          sx={(theme) => ({
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            width: "100%",
            mt: "-100px",
            [theme.breakpoints.down("md")]: {
              mt: "-40px",
            },
            [theme.breakpoints.down("sm")]: {
              mt: "40px",
              flexDirection: "column-reverse",
              justifyContent: "center",
            },
          })}
        >
          <Typography
            variant="dr"
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                mt: "20px",
              },
            })}
          >
            dr stomatologije <br /> Tanja Toroman
          </Typography>
          <Box
            sx={(theme) => ({
              position: "relative",
              width: "400px",
              height: "400px",
              borderRadius: "50%",
              overflow: "hidden",
              ml: "120px",
              [theme.breakpoints.down("md")]: {
                ml: "80px",
                width: "300px",
                height: "300px",
              },
              [theme.breakpoints.down("sm")]: {
                ml: "0px",
                flexDirection: "column-reverse",
                justifyContent: "center",
              },
            })}
          >
            {/* <picture>
          <source srcSet="/images/tanja.webp" media="(min-width: 420px)" /> */}
            <Image
              alt="dr Tanja Toroman"
              src={"/images/tanja.webp"}
              fill
              loading="lazy"
              style={{
                objectFit: "cover",
              }}
            />
            {/* </picture> */}
          </Box>
        </Box>
        <Box
          sx={(theme) => ({
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
            width: "100%",
            mt: "-60px",
            [theme.breakpoints.down("md")]: {
              mt: "-40px",
            },
            [theme.breakpoints.down("sm")]: {
              mt: "40px",
              flexDirection: "column",
              justifyContent: "center",
            },
          })}
        >
          <Box
            sx={(theme) => ({
              position: "relative",
              width: "400px",
              height: "400px",
              borderRadius: "50%",
              overflow: "hidden",
              flexShrink: 0,
              mr: "120px",
              [theme.breakpoints.down("md")]: {
                mr: "80px",
                width: "320px",
                height: "320px",
              },
              [theme.breakpoints.down("sm")]: {
                mr: "0px",
                width: "300px",
                height: "300px",
              },
            })}
          >
            {/* <picture>
          <source srcSet="/images/tijana.webp" media="(min-width: 420px)" /> */}
            <Image
              alt="Nikolina Vasić"
              src={"/images/nikolina.webp"}
              fill
              loading="lazy"
              style={{
                objectFit: "cover",
              }}
            />
            {/* </picture> */}
          </Box>
          <Typography
            variant="dr"
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                mt: "20px",
              },
            })}
          >
            stomatološka sestra <br /> Nikolina Vasić
          </Typography>
        </Box>
      </>
    </SectionWrapper>
  );
};

export default Team;
