import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../common/SectionWrapper";
import Grid from "@mui/material/Grid";
import LocalPhoneRoundedIcon from "@mui/icons-material/LocalPhoneRounded";
import WatchLaterRoundedIcon from "@mui/icons-material/WatchLaterRounded";
import { Typography } from "@mui/material";
import Image from "next/image";
import AlternateEmailRoundedIcon from "@mui/icons-material/AlternateEmailRounded";
import { handleAnalytics } from "../utils/analytics";
import ObserverLazyLoad from "../common/ObserverLazyLoad";

interface ContactProps {}

const social = [
  { title: "Viber_c", href: "viber://chat?number=38766140142" },
  { title: "WhatsApp_c", href: "whatsapp://send?phone=38766140142" },
  {
    title: "Instagram_c",
    href: "https://www.instagram.com/stomatologija_toroman",
  },
  {
    title: "Facebook_c",
    href: "https://www.facebook.com/people/Stomatologija-Toroman/61556117855824",
  },
];

const Contact: FC<ContactProps> = (): ReactElement => {
  const { render, ref } = ObserverLazyLoad();
  return (
    <SectionWrapper title="KONTAKT" id="contact">
      <Grid
        ref={ref}
        container
        pb="60px"
        columnSpacing={4}
        rowSpacing={3}
        minHeight="400px"
        sx={(theme) => ({
          pb: "60px",
          [theme.breakpoints.down("sm")]: { pb: "20px" },
        })}
      >
        {render && (
          <>
            <Grid
              item
              sx={(theme) => ({
                width: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                mb: "50px",
                [theme.breakpoints.down("sm")]: {
                  mb: "10px",
                },
              })}
            >
              <Typography maxWidth="860px" textAlign="center">
                Nalazimo se u naselju Petrićevac (Žute zgrade) na adresi
                Rakovačkih rudara 10. <br />
                <br /> Našim pacijentima obezbjeđen je
                <br />
                <strong>besplatan parking</strong>.
              </Typography>
            </Grid>
            <Grid
              item
              xs={12}
              sm={6}
              md={5}
              sx={(theme) => ({
                width: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "flex-start",
                flexDirection: "column",
              })}
            >
              <Box
                sx={(theme) => ({
                  borderRadius: "8px",
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                })}
              >
                <LocalPhoneRoundedIcon
                  fontSize="large"
                  color="primary"
                  sx={(theme) => ({
                    mr: "20px",
                    [theme.breakpoints.down("sm")]: {
                      mr: "14px",
                    },
                  })}
                />
                <Typography variant="body1">+387 66 140 142</Typography>
              </Box>

              <Box
                sx={(theme) => ({
                  borderRadius: "8px",
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  my: "50px",
                  [theme.breakpoints.down("md")]: {
                    my: "30px",
                  },
                  [theme.breakpoints.down("sm")]: {
                    my: "15px",
                  },
                })}
              >
                <AlternateEmailRoundedIcon
                  fontSize="large"
                  color="primary"
                  sx={(theme) => ({
                    mr: "20px",
                    [theme.breakpoints.down("sm")]: {
                      mr: "14px",
                    },
                  })}
                />

                <Typography variant="body1">
                  stomatologijatoroman@gmail.com
                </Typography>
              </Box>
              <Box
                sx={(theme) => ({
                  borderRadius: "8px",
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                })}
              >
                <WatchLaterRoundedIcon
                  fontSize="large"
                  color="primary"
                  sx={(theme) => ({
                    mr: "20px",
                    [theme.breakpoints.down("sm")]: {
                      mr: "14px",
                    },
                  })}
                />
                <Typography variant="body1" textAlign="left">
                  <strong>Pon, Sri, Pet: </strong>
                  12-20h <br />
                  <strong>Uto, Cet: </strong>
                  09-17h
                </Typography>
              </Box>

              <Box
                sx={(theme) => ({
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-around",
                  alignItems: "center",
                  px: "40px",
                  mt: "50px",
                  [theme.breakpoints.down("md")]: {
                    mt: "30px",
                  },
                  [theme.breakpoints.down("sm")]: {
                    mt: "38px",
                    mb: "16px",
                    px: "12px",
                  },
                })}
              >
                {social.map(({ title, href }, i) => (
                  <a
                    key={title}
                    href={href}
                    target="_blank"
                    onClick={() => handleAnalytics(3000 + i)}
                    rel="noreferrer"
                  >
                    <Box
                      key={title}
                      sx={{
                        position: "relative",
                        width: "calc(36px + 1vw)",
                        height: "calc(36px + 1vw)",
                        cursor: "pointer",
                        opacity: 0.9,
                      }}
                    >
                      <Image
                        alt={title}
                        src={`/images/${title}.svg`}
                        fill
                        title={title}
                      />
                    </Box>
                  </a>
                ))}
              </Box>
            </Grid>
            <Grid item xs={12} sm={6} md={7} display="flex">
              <Box
                sx={(theme) => ({
                  width: "100%",
                  height: "370px",
                  [theme.breakpoints.down("md")]: {
                    height: "280px",
                  },
                  [theme.breakpoints.down("sm")]: {
                    height: "220px",
                  },
                })}
              >
                <iframe
                  title="google location"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1415.6737223824932!2d17.191816538967462!3d44.79410739618012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475e030064ac7ce7%3A0xce484c655945db80!2sStomatologija%20Toroman!5e0!3m2!1sen!2sfr!4v1708361421947!5m2!1sen!2sfr&loading=async"
                  width="100%"
                  height="100%"
                  style={{ border: "0" }}
                  loading="lazy"
                ></iframe>
              </Box>
            </Grid>
          </>
        )}
      </Grid>
    </SectionWrapper>
  );
};

export default Contact;
