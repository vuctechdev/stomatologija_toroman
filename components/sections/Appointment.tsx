import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import Image from "next/image";
import AppointmentButton from "../common/AppointmentButton";

interface AppointmentProps {}

const Appointment: FC<AppointmentProps> = (): ReactElement => {
  return (
    <Box
      sx={(theme) => ({
        width: 1,
        height: "350px",
        position: "relative",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        mt: "100px",
        mb: "20px",
        "::after": {
          content: '""',
          position: "absolute",
          top: "0px",
          left: "0px",
          width: "100%",
          height: "100%",
          zIndex: 100,
          background:
            "linear-gradient(90deg, rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0.1))",
        },
        [theme.breakpoints.down("md")]: {
          height: "280px",
          mt: "100px",
        },
        [theme.breakpoints.down("sm")]: {
          height: "230px",
          mt: "80px",
        },
      })}
    >
      <picture>
        <source srcSet="/images/appointment.webp" media="(min-width: 800px)" />
        <Image
          alt="Appointment"
          src={"/images/appointment_mini.webp"}
          fill
          loading="lazy"
          style={{
            objectFit: "cover",
          }}
        />
      </picture>
      <AppointmentButton />
    </Box>
  );
};

export default Appointment;
