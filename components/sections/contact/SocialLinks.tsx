import React, { FC, ReactElement } from "react";
import { Box, Grid } from "@mui/material";
import Image from "next/image";
import { mediaBaseURL } from "../../common/data/config";
import { handleAnalytics } from "../../utils/analytics";

const socialData = [
  {
    href: "viber://chat?number=38766140142",
    title: "Viber",
  },
  {
    href: "https://www.instagram.com/mt__architecture/",
    title: "Instagram",
  },
  {
    href: "https://www.facebook.com/MTarchitecture1",
    title: "Facebook",
  },
  // {
  //   href: 'https://www.upwork.com/freelancers/~01625633bf1b01aee1',
  //   title: 'UpWork',
  // },
  {
    href: "https://www.linkedin.com/in/milan-te%C5%A1anovi%C4%87-39a884210/",
    title: "Linkedin",
  },
];

interface SocialLinksProps {}

const SocialLinks: FC<SocialLinksProps> = (): ReactElement => {
  const handleClick = async (code: number) => {
    handleAnalytics(code);
  };
  return (
    <Grid
      item
      lg={6}
      md={12}
      xs={12}
      display="flex"
      sx={(theme) => ({
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        backgroundColor: theme.palette.primary.main,
        [theme.breakpoints.down("lg")]: {
          "&:first-child": {
            marginBottom: "100px",
          },
          "&:last-child": {
            maxHeight: "50px",
          },
        },
        "&> h5": {
          width: "auto",
        },
        "&> a": {
          margin: "0 1.5vw",
        },
      })}
    >
      {socialData.map((data, i) => (
        <a
          key={data.title}
          href={data.href}
          target="_blank"
          onClick={() => handleClick(30000 + i)}
          onAuxClick={() => handleClick(30000 + i)}
          rel="noreferrer"
        >
          <Box
            sx={{
              position: "relative",
              width: "calc(40px + 2vw)",
              height: "calc(40px + 2vw)",
            }}
          >
            <Image
              alt={data.title}
              src={`${mediaBaseURL}/social/${data.title}.webp`}
              fill
              title={data.title}
            />
          </Box>
        </a>
      ))}
    </Grid>
  );
};

export default SocialLinks;
