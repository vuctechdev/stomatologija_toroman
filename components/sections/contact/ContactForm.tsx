import React, { FC, ReactElement, useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import FormFields from "./FormFields";
import CircularProgress from "@mui/material/CircularProgress";
import MarkEmailReadIcon from "@mui/icons-material/MarkEmailRead";
import { apiBaseUrl } from "../../common/data/config";
import Dialog from "@mui/material/Dialog";

const initialValues = {
  title: "",
  email: "",
  message: "",
};

const validationSchema = Yup.object().shape({
  title: Yup.string().required("Obavezno polje"),
  email: Yup.string().email("Email nije validan").required("Obavezno polje"),
  message: Yup.string().required("Obavezno polje"),
});

export type InitialValues = typeof initialValues;

interface ContactFormProps {
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const ContactForm: FC<ContactFormProps> = ({ closeModal }): ReactElement => {
  const [sent, setSent] = useState(false);

  const handleSubmit = async (values: InitialValues) => {
    try {
      await fetch(`${apiBaseUrl}/contact`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        method: "POST",
        body: JSON.stringify(values),
      });
      setSent(true);
    } catch (err) {}
  };

  useEffect(() => {
    if (sent) {
      setTimeout(() => {
        closeModal(false);
      }, 2000);
    }
  }, [sent, closeModal]);

  return (
    <Dialog
      open={true}
      onClose={() => closeModal(false)}
      maxWidth="sm"
      fullScreen={window.innerWidth < 600}
    >
      <Box p="40px" display="flex" alignItems="center" height={1}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <Grid container>
                <FormFields />
              </Grid>
              <Box
                m="35px 0 1px"
                width={1}
                display="flex"
                justifyContent="flex-end"
              >
                <Button
                  disabled={isSubmitting}
                  variant="outlined"
                  onClick={() => closeModal(false)}
                  fullWidth
                >
                  Otkaži
                </Button>
                <Box width="20px" />
                <Button
                  disabled={isSubmitting}
                  variant="contained"
                  type="submit"
                  fullWidth
                  color={sent ? "success" : "primary"}
                  sx={{
                    fontWeight: 600,
                    "& circle": { color: "rgba(0,0,0,0.9)" },
                  }}
                >
                  {isSubmitting ? (
                    <CircularProgress size={22} />
                  ) : sent ? (
                    <MarkEmailReadIcon />
                  ) : (
                    "Pošalji"
                  )}
                </Button>
              </Box>
            </Form>
          )}
        </Formik>
      </Box>
    </Dialog>
  );
};

export default ContactForm;
