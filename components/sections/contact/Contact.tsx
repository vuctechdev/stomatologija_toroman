import { Box, Button, Divider, Typography, Grid } from "@mui/material";
import React, { FC, ReactElement, useState } from "react";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import dynamic from "next/dynamic";
import SocialLinks from "./SocialLinks";
import SectionWrapper from "../../common/SectionWrapper";

const ContactForm = dynamic(() => import("./ContactForm"));
// const SocialLinks = dynamic(() => import("./SocialLinks"), {
//   loading: () => <p>Loading...</p>,
//   ssr: false,
// });

const Footer: FC = (): ReactElement => {
  const [openForm, setOpenForm] = useState(false);

  return (
    // <Box width={1} p="0 5vw" id="contact">
    //   <Typography variant="h1" textAlign="left">
    //     KONTAKT
    //   </Typography>
    //   <Divider />
    //   <Box width={1} p="10vh 1vw">
    //     <Grid container>
    //       <Grid
    //         item
    //         lg={6}
    //         md={12}
    //         xs={12}
    //         sx={(theme) => ({
    //           display: "flex",
    //           justifyContent: "center",
    //           alignItems: "center",
    //           [theme.breakpoints.down("lg")]: {
    //             "&:first-child": {
    //               marginBottom: "100px",
    //             },
    //             "&:last-child": {
    //               maxHeight: "50px",
    //             },
    //           },
    //           "&> h5": {
    //             width: "auto",
    //           },
    //           "&> a": {
    //             margin: "0 1.5vw",
    //           },
    //         })}
    //         flexDirection="column"
    //       >
    //         <Typography variant="h2">
    //           Stomatološka ordinacija TOROMAN
    //         </Typography>
    //         <Box m="12px 0">
    //           <Typography variant="h3">Rakovačkih rudara 10</Typography>
    //         </Box>
    //         <Box m="12px 0">
    //           <Typography variant="h3">066-140-142</Typography>
    //         </Box>
    //         pon sre pet 12-20h
    //         uto-cet 09-17

    //         <Typography variant="h4">stomatologijatoroman@gmail.com</Typography>
    //         <Button
    //           onClick={() => setOpenForm(true)}
    //           startIcon={<MailOutlineIcon fontSize="large" />}
    //           variant="contained"
    //           sx={{
    //             color: "black !important",
    //             marginTop: "25px",
    //             "& > span > svg > path": {
    //               color: "black !important",
    //             },
    //           }}
    //         >
    //           PoŠalji Emajl
    //         </Button>
    //       </Grid>
    //       <SocialLinks />
    //       {openForm && <ContactForm closeModal={setOpenForm} />}
    //     </Grid>
    //   </Box>
    // </Box>

    <SectionWrapper title="KONTAKT" id="contact">
      <Box width={1} p="10vh 1vw">
        <Grid container>
          <Grid
            item
            lg={6}
            md={12}
            xs={12}
            sx={(theme) => ({
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              [theme.breakpoints.down("lg")]: {
                "&:first-child": {
                  marginBottom: "100px",
                },
                "&:last-child": {
                  maxHeight: "50px",
                },
              },
              "&> h5": {
                width: "auto",
              },
              "&> a": {
                margin: "0 1.5vw",
              },
            })}
            flexDirection="column"
          >
            <Typography variant="h3">
              Stomatološka ordinacija TOROMAN
            </Typography>
            <Box m="12px 0">
              <Typography variant="h4">Rakovačkih rudara 10</Typography>
            </Box>
            <Box m="12px 0">
              <Typography variant="h4">066-140-142</Typography>
            </Box>
            pon sre pet 12-20h uto-cet 09-17
            <Typography variant="h5">stomatologijatoroman@gmail.com</Typography>
            <Button
              onClick={() => setOpenForm(true)}
              startIcon={<MailOutlineIcon fontSize="large" />}
              variant="contained"
              sx={{
                color: "black !important",
                marginTop: "25px",
                "& > span > svg > path": {
                  color: "black !important",
                },
              }}
            >
              PoŠalji Emajl
            </Button>
          </Grid>
          <SocialLinks />
          {openForm && <ContactForm closeModal={setOpenForm} />}
        </Grid>
      </Box>
    </SectionWrapper>
  );
};

export default Footer;
