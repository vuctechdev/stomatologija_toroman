import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { useFormikContext } from "formik";
import React, { FC, ReactElement } from "react";
import { InitialValues } from "./ContactForm";

interface FormFieldsProps {}

const FormFields: FC<FormFieldsProps> = (): ReactElement => {
  const { touched, errors, getFieldProps } = useFormikContext<InitialValues>();

  const getErrorMessage = (name: "title" | "email" | "message") => {
    if (touched[name] && errors[name]) {
      return errors[name];
    }
    return "";
  };

  return (
    <>
      <TextField
        fullWidth
        label="Naslov"
        {...getFieldProps("title")}
        error={touched.title && !!errors.title}
        helperText={getErrorMessage("title")}
        sx={{
          "& p": {
            fontWeight: 700,
            textAlign: "right",
            fontSize: "12px",
            lineHeight: "16px",
            letterSpacing: "0.2px",
          },
        }}
      />
      <Box m="18px 0" width={1}>
        <TextField
          fullWidth
          label="Email"
          {...getFieldProps("email")}
          error={touched.email && !!errors.email}
          helperText={getErrorMessage("email")}
          sx={{
            "& p": {
              fontWeight: 700,
              textAlign: "right",
              fontSize: "12px",
              lineHeight: "16px",
              letterSpacing: "0.2px",
            },
          }}
        />
      </Box>
      <TextField
        multiline
        rows={8}
        fullWidth
        label="Poruka"
        {...getFieldProps("message")}
        error={touched.message && !!errors.message}
        helperText={getErrorMessage("message")}
        sx={{
          "& p": {
            fontWeight: 700,
            textAlign: "right",
            fontSize: "12px",
            lineHeight: "16px",
            letterSpacing: "0.2px",
          },
        }}
      />
    </>
  );
};

export default FormFields;
