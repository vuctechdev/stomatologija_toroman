import Box from "@mui/material/Box";
import React, { FC, ReactElement } from "react";
import Image from "next/image";
import { Typography } from "@mui/material";

const Hero: FC = (): ReactElement => {
  return (
    <Box
      position="relative"
      overflow="hidden"
      sx={(theme) => ({
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        mt: "65px",
        backgroundColor: theme.palette.primary.main,
        width: "100%",
        height: "92vh",
        pt: "290px",
        [theme.breakpoints.down("sm")]: {
          height: "74vh",
          maxHeight: "700px",
          mb: "40px",
          pt: "160px",
        },
      })}
    >
      <Box
        sx={(theme) => ({
          top: "0px",
          left: "0px",
          paddingTop: "40px",
          maxHeight: "1100px",
        })}
      >
        <Box width={1} height={1} zIndex={-1} sx={{ opacity: 0.25 }}>
          <picture>
            <source srcSet="/images/mica.webp" media="(min-width: 600px)" />
            <Image
              alt="Hero"
              priority={true}
              src={"/images/mica_min.webp"}
              fill
              loading="eager"
              style={{
                objectFit: "cover",
              }}
            />
          </picture>
        </Box>
        <Box
          sx={(theme) => ({
            width: "489px",
            height: "206px",
            position: "relative",
            [theme.breakpoints.down("sm")]: {
              width: "320px",
              height: "135px",
              ml: "8px",
            },
          })}
        >
          <picture style={{ zIndex: 2 }}>
            <source srcSet="/images/logo.svg" media="(min-width: 760px)" />
            <Image
              alt="Logo"
              src={"/images/logo_min.svg"}
              fill
              loading="lazy"
              style={{
                objectFit: "cover",
              }}
            />
          </picture>
        </Box>
        <Typography
          sx={(theme) => ({
            mt: "100px",
            position: "relative",
            zIndex: 100,
            opacity: 0.9,
            [theme.breakpoints.down("sm")]: {
              mt: "60px",
            },
          })}
          variant="h1"
        >
          Tamo gdje osmijeh oživi!
        </Typography>
      </Box>
    </Box>
  );
};

export default Hero;
