import Hero from "../components/sections/Hero";
import Services from "../components/sections/Services";
import PageHead from "../components/common/PageHead";
import Navigation from "../components/common/navigation/Navigation";
import Contact from "../components/sections/Contact";
import {
  description,
  favicon,
  title,
  webURL,
} from "../components/common/data/config";
import {
  otherProjects,
  highlightedProjects,
} from "../components/common/data/items";
import { FC } from "react";
import Appointment from "../components/sections/Appointment";
import About from "../components/sections/About";
import {
  handleAnalytics,
  insertAnalytics,
} from "../components/utils/analytics";
import Team from "../components/sections/Team";

interface HomeProps {
  highlightedProjects: any[];
  otherProjects: any[];
}

handleAnalytics(1000);

const Home: FC<HomeProps> = ({ highlightedProjects, otherProjects }) => {
  return (
    <>
      <PageHead
        title={title}
        description={description}
        webURL={webURL}
        favicon={favicon}
        image="/images/ogImage.webp"
      />
      <main>
        <Navigation />
        <Hero />
        <About />

        <Services />
        <Appointment />
        <Team />
        <Contact />
      </main>
    </>
  );
};

export default Home;

export function getStaticProps() {
  return {
    props: {
      otherProjects: otherProjects,
      highlightedProjects: highlightedProjects,
    },
  };
}
