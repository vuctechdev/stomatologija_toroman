import PageHead from "../components/common/PageHead";
import Navigation from "../components/common/navigation/Navigation";
import {
  description,
  favicon,
  title,
  webURL,
} from "../components/common/data/config";
import AppointmentForm from "../components/common/AppointmentForm";
import { handleAnalytics } from "../components/utils/analytics";

handleAnalytics(1001);

const Appointment = () => {
  return (
    <>
      <PageHead
        title={title}
        description="Zakažite termin u našoj stomatološkoj ordinaciji"
        webURL={webURL}
        favicon={favicon}
        image="/images/ogImage.webp"
      />
      <main>
        <Navigation />
        <AppointmentForm />
      </main>
    </>
  );
};

export default Appointment;
